This is simple SwiftUI and Core Data application that fetch list of advertisments from the given API. 
Its shows a collection view type of ad listing in the first screen that mimic the current finn.no website. 
It shows thumbnail of images, text location, total price and short description. You can save your selected
advertisments to local storage and can show it using the toggle switch located in the top right portion of the 
screen.

It uses standard swift code without 3rd party libraries. 

**Requirements:**
1. Xcode 13.1
2. Minimum deployment:  iOS 15.0


**Something I am proud off:**
1. _I enjoyed and learned a lot from this exercise._
2. _It gives me a different perspective when using Core Data with SwiftUI_


**Is there anything I could have done better and I could have done with more:**
1. _I am not currently satisfied with the code and it can be cleaner, could have a better architecture since I use my existing SwiftUI knowledge and and I know it is still work in progress. I also got confused of how to combine a normal model to a NSManagedObject or just map it manually but I proceed with mapping it manually since I am a bit catching up to the time/deadline._
2. _I have some issues with ObservedObjects/EnvironmentObjects, I was reflecting in the user interface on a dummy project but when I put it on this project it seems not refreshing accordingly._
3. Deleting of Favorite



Screenshot:

<img src="image1.png" width="200">
<img src="image2.png" width="200">
