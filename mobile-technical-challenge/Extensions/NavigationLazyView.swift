//
//  NavigationLazyView.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 15/11/2021.
//
// From: https://stackoverflow.com/questions/57594159/swiftui-navigationlink-loads-destination-view-immediately-without-clicking

import Foundation
import SwiftUI

struct NavigationLazyView<Content: View>: View {
    let build: () -> Content
    init(_ build: @autoclosure @escaping () -> Content) {
        self.build = build
    }
    var body: Content {
        build()
    }
}
