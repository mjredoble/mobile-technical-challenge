//
//  AdService.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation

final class AdService {
    
    static var API_URL = "https://gist.githubusercontent.com/baldermork/6a1bcc8f429dcdb8f9196e917e5138bd/raw/discover.json"
    static var API_IMAGE_BASE_URL = "https://images.finncdn.no/dynamic/480x360c/"
    
    static func getListOfAds(completionHandler: @escaping ([Ad]) -> (), errorHandler: @escaping () -> ()) {
        let apiService = APIRequest()
        
        let urlString = API_URL
        apiService.httpMethod = "GET"
        
        apiService.execute(url: urlString, completionHandler: { result in
            if let data = result {
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    let list = try decoder.decode(Items.self, from: data)
                    completionHandler(list.items)
                }
                catch {
                    print(error)
                    errorHandler()
                }
            }
        })
    }
    
    static func getPreviewListOfAds() -> [Ad] {
        guard let fileUrl = Bundle.main.url(forResource: "data", withExtension: "json"), let data = try? Data(contentsOf: fileUrl) else {
            fatalError()
        }
        
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            
            let list = try decoder.decode(Items.self, from: data)
            return list.items
        }
        catch {
            return [Ad]()
        }
    }
    
    static func getPreviewDetailData() -> Ad {
        let list = AdService.getPreviewListOfAds()
        return list[0]
    }
}
