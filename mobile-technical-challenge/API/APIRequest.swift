//
//  ApiService.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation

final class APIRequest {
    var baseURL = ""
      var httpMethod: String?
      var postData: NSMutableDictionary?
      var responseData: Data?
      var timeout = 0
      var responseCode = 0

    func execute(url: String, completionHandler: @escaping (Data?) -> ()) {
            let url = URL(string: baseURL + url)!

            var request = URLRequest(url: url)
            request.timeoutInterval = TimeInterval(timeout)
            request.httpMethod = httpMethod
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            if let postData = postData,
               let httpBody = try? JSONSerialization.data(withJSONObject: postData, options: [.prettyPrinted]) {
                request.httpBody = httpBody
            }
            
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                if let error = error {
                    print("HTTP Request Failed \(error)")
                }
                
                if let httpResponse = response as? HTTPURLResponse {
                    self.responseCode = httpResponse.statusCode
                    print("statusCode: \(httpResponse.statusCode)")
                }
                
                if let data = data {
                    completionHandler(data)
                }
            }
            
            task.resume()
        }
}
