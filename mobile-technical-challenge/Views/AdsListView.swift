//
//  MainView.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import SwiftUI
import CoreData

struct AdsListView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @EnvironmentObject private var viewModel: AdViewModel
    
    @State var listOfAds = [Ad]()
    @State private var showingAlert = false
    @State private var showFavorites = false
    
    var body: some View {
        NavigationView {
            ScrollView {
                HStack {
                    Spacer()
                    Text("Show Favorites:")
                        .foregroundColor(Color.black)
                        .font(.caption)
                        
                    Toggle("",
                           isOn: $showFavorites)
                        .onChange(of: showFavorites) { value in
                            self.showFavorites = value
                            
                            //Set to show offline data when Connectivity is off
                            if !viewModel.isConnected {
                                self.showFavorites = true
                            }
                            
                            //TODO: It needs refactoring, I tried this method to make it work
                            if self.showFavorites {
                                DispatchQueue.main.async {
                                    self.listOfAds.removeAll()
                                    self.listOfAds.append(contentsOf: viewModel.getFavorites())
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                    self.listOfAds.removeAll()
                                    self.viewModel.getListOfAds(completionHandler: { result in
                                        self.listOfAds.append(contentsOf: result)
                                    })
                                }
                            }
                        }
                        .labelsHidden()
                        
                }
                .padding(.trailing, 10)
                
                if let listOfAds = listOfAds {
                    AdsCollectionView(listOfAds: listOfAds, didSelectForFavorite: viewModel.saveAdToFavorite)
                }
            }
            .background(Color.white)
            .onAppear {
                if viewModel.isConnected {
                    viewModel.getListOfAds(completionHandler: { listOfAds in
                        self.listOfAds = listOfAds
                    })
                }
                else {
                    self.showFavorites = true
                    self.listOfAds = viewModel.getFavorites()
                }
            }
            .navigationBarTitleDisplayMode(.large)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    HStack {
                        Text("Populære Annonser")
                            .font(.title)
                            .foregroundColor(.black)
                    }
                }
            }
        }
        .alert(viewModel.errorMessage , isPresented: $showingAlert) {
            Button("OK", role: .cancel) { }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        AdsListView()
    }
}
