//
//  AdsDetailView.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 15/11/2021.
//

import SwiftUI

struct AdsDetailView: View {
    @EnvironmentObject private var viewModel: AdViewModel
    var adItem: Ad
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            if let imageURL = adItem.image?.url {
                AsyncImage(url: URL(string:"\(AdService.API_IMAGE_BASE_URL)\(imageURL)")) { image in
                    image
                        .resizable()
                        .scaledToFit()
                } placeholder: {
                    SwiftUI.Image("placeholder")
                        .frame(maxWidth: .infinity, maxHeight: 150)
                        .opacity(0.05)
                        .cornerRadius(16)
                        .padding(.bottom, 10)
                }
                .background(Color("Bliue-100"))
                .frame(maxWidth: .infinity, maxHeight: 300)
                .cornerRadius(16)
            }
            
            Text(adItem.location ?? "")
                .foregroundColor(Color.black)
                .font(.body)
                .padding([.bottom], 10)
                .padding([.top], 10)
            
            Text(adItem.description ?? "")
                .foregroundColor(Color.black)
                .font(.body)
                .fontWeight(.semibold)
                .multilineTextAlignment(.leading)
            
            Text(viewModel.totalPriceText(total: adItem.price?.total))
                .foregroundColor(Color.black)
                .font(.body)
                .padding([.top, .bottom], 10)
            
            Spacer()
        }
        .padding()
        .background(Color.white)
    }
}

struct AdsDetailView_Previews: PreviewProvider {
    static var previews: some View {
        AdsDetailView(adItem: AdService.getPreviewDetailData())
    }
}
