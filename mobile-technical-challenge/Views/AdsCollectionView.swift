//
//  AdsCollectionView.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 15/11/2021.
//

import SwiftUI

struct AdsCollectionView: View {
    var listOfAds: [Ad]
    var didSelectForFavorite: (Ad)-> Void
    
    var columns = [
        GridItem(.adaptive(minimum: 100), spacing: 10),
        GridItem(.adaptive(minimum: 100), spacing: 10),
    ]
    
    var body: some View {
        LazyVGrid(columns: columns, spacing: 0) {
            if let listOfAds = listOfAds {
                ForEach(listOfAds) { item in
                    NavigationLink(destination: NavigationLazyView(AdsDetailView(adItem: item))) {
                        VStack(alignment: .leading, spacing: 0) {
                            ZStack(alignment: .top) {
                                VStack {
                                    if let imageURL = item.image?.url {
                                        AsyncImage(url: URL(string:"\(AdService.API_IMAGE_BASE_URL)\(imageURL)")) { image in
                                            image
                                                .resizable()
                                                .scaledToFill()
                                        } placeholder: {
                                            SwiftUI.Image("placeholder")
                                                .frame(maxWidth: 150, maxHeight: 150)
                                                .opacity(0.05)
                                                .cornerRadius(16)
                                                .padding(.bottom, 5)
                                        }
                                        .background(Color("Bliue-100"))
                                        .frame(maxWidth: 160, maxHeight: 160)
                                        .cornerRadius(16)
                                    }
                                }
                                
                                HStack {
                                    Spacer()
                                    Spacer()

                                    Button(action: {
                                        self.didSelectForFavorite(item)
                                    }) {
                                        SwiftUI.Image(item.isFavorite ?? false ? "favorite" : "not-favorite")
                                            .resizable()
                                            .frame(width: 24, height: 24, alignment: .trailing)
                                            .padding(10)
                                    }
                                }
                            }
                            
                            Text(item.location ?? "")
                                .foregroundColor(Color.black)
                                .font(.caption2)
                                .lineLimit(1)
                                .padding([.bottom], 5)
                                .padding([.top], 10)
                            
                            Text(item.description ?? "")
                                .foregroundColor(Color.black)
                                .font(.caption)
                                .fontWeight(.semibold)
                                .lineLimit(1)
                                .multilineTextAlignment(.leading)
                            
                            Text(totalPriceText(total: item.price?.total))
                                .foregroundColor(Color.black)
                                .font(.caption2)
                                .lineLimit(2)
                                .padding([.top, .bottom], 5)
                        }
                        .frame(width: 144, height: 254, alignment: .leading)
                    }
                }
            }
        }
    }
    
    func totalPriceText(total: Int?) -> String {
        if let total = total {
            return "Totalpris: \(total)"
        }
        else {
            return "\t"
        }
    }
}

struct AdsCollectionView_Previews: PreviewProvider {
    static var previews: some View {
        AdsCollectionView(listOfAds: AdService.getPreviewListOfAds(), didSelectForFavorite: { _ in
            //
        })
    }
}
