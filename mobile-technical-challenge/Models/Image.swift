//
//  Image.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation

final class Image: Codable, Identifiable {
    var url: String?
    var height: Int?
    var width: Int?
    var type: String?
    var scalable: Bool?
    
    init(url: String) {
        self.url = url
    }
}
