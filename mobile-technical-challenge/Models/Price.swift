//
//  Price.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation

final class Price: Codable, Identifiable {
    var value: Int?
    var total: Int?
    
    
    init(value: Int, total: Int) {
        self.value = value
        self.total = total
    }
}
