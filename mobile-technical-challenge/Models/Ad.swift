//
//  Ad.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation

final class Ad: Codable, Identifiable {
    var description: String?
    var id: String
    var adType: String?
    var location: String?
    var type: String?
    var price: Price?
    var image: Image?
    var tracking: Tracking?
    var score: Float?
    var version: String?
    var isFavorite: Bool?
    
    enum CodingKeys: String, CodingKey {
        case description = "description"
        case id = "id"
        case adType = "ad-type"
        case location = "location"
        case type = "type"
        case price = "price"
        case image = "image"
        case tracking = "tracking"
        case score = "score"
        case version = "version"
    }
    
    init(id: String,
         description: String,
         location: String,
         adType: String,
         imageUrl: String,
         value: Int,
         total: Int,
         type: StringLiteralType) {
        self.id = id
        self.description = description
        self.location = location
        self.adType = adType
        self.price = Price(value: value, total: total)
        self.image = Image(url: imageUrl)
      }
}
