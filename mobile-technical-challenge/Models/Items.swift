//
//  Items.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation

final class Items: Codable, Identifiable {
    var items: [Ad]
}
