//
//  Tracking.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation

final class Tracking: Codable, Identifiable {
    var ec: [String: [String]]?
    var click: [String]?
    var adobe: [String: String]?
    var actions: [String: [String]]?
}
