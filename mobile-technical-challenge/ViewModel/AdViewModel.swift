//
//  AdViewModel.swift
//  mobile-technical-challenge
//
//  Created by Michael Joseph Redoble on 14/11/2021.
//

import Foundation
import CoreData
import Network

final class AdViewModel: ObservableObject {
    
    private let context: NSManagedObjectContext
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "Monitor")
    
    @Published var listOfAds: [Ad]
    @Published var favorites: [Favorite]
    @Published var errorMessage = ""
    @Published var isConnected = true
    
    init(context: NSManagedObjectContext) {
        self.context = context
        self.favorites = [Favorite]()
        self.listOfAds = [Ad]()
        
        do {
            let array = try context.fetch(NSFetchRequest<Favorite>(entityName: "Favorite"))
            self.favorites.append(contentsOf: array)
        }
        catch {
            print("error FetchRequest \(error)")
        }
        
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.async {
                self.isConnected = path.status == .satisfied ? true : false
            }
        }
        monitor.start(queue: queue)
    }
    
    func getListOfAds(completionHandler: @escaping ([Ad]) -> ()) {
        AdService.getListOfAds(completionHandler: { result in
            DispatchQueue.main.async {
                self.listOfAds.removeAll()
                self.listOfAds.append(contentsOf: result)
                
                for item in self.listOfAds {
                    if self.isFavorite(ad: item) {
                        item.isFavorite = true
                    }
                }
                    
                self.objectWillChange.send()
                completionHandler(result)
            }
        }, errorHandler: {
            DispatchQueue.main.async {
                self.errorMessage = "Oops something went wrong, please try again laer"
                self.objectWillChange.send()
            }
        })
    }
    
    func getFavorites() -> [Ad] {
        self.favorites.removeAll()
        
        do {
            let array = try context.fetch(NSFetchRequest<Favorite>(entityName: "Favorite"))
            self.favorites.append(contentsOf: array)
        }
        catch {
            print("error FetchRequest \(error)")
        }
        
        var list = [Ad]()
        
        for item in self.favorites {
            let ad = Ad(id: item.id ?? "",
                        description: item.desc ?? "",
                        location: item.location ?? "",
                        adType: item.adType ?? "",
                        imageUrl: item.imageUrl ?? "",
                        value: Int(item.value),
                        total: Int(item.total),
                        type: item.type ?? "")
            ad.isFavorite = true
            list.append(ad)
        }
        
        return list
    }
    
    func isFavorite(ad: Ad) -> Bool {
        
        for item in getFavorites() {
            if item.id == ad.id {
                return true
            }
        }
        
        return false
    }
    
    func saveAdToFavorite(ad: Ad) {
        let newItem = Favorite(context: context)
        newItem.id = ad.id
        newItem.desc = ad.description ?? ""
        newItem.location = ad.location ?? ""
        newItem.adType = ad.adType ?? ""
        newItem.imageUrl = ad.image?.url
        newItem.total = Int64(ad.price?.total ?? 0)
        newItem.value = Int64(ad.price?.value ?? 0)
        newItem.type = ad.type
        
        for item in self.listOfAds {
            if item.id == ad.id {
                item.isFavorite = !(ad.isFavorite ?? false) ? true : false
            }
        }
        
        do {
            try context.save()
        } catch {
            let nsError = error as NSError
            self.errorMessage = "Unresolved error \(nsError)"
        }
        
        self.objectWillChange.send()
    }
    
    func totalPriceText(total: Int?) -> String {
        if let total = total {
            return "Totalpris: \(total)"
        }
        else {
            return "\t"
        }
    }
}
